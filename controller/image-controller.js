import grid from 'gridfs-stream';
import mongoose from 'mongoose';
const url = 'http://localhost:8000';
import Post from '../model/post.js';

let gfs, gridfsBucket;
const conn = mongoose.connection;
conn.once('open', () => {
    gridfsBucket = new mongoose.mongo.GridFSBucket(conn.db, {
        bucketName: 'fs'
    });
    gfs = grid(conn.db, mongoose.mongo);
    gfs.collection('fs');
});


export const uploadImage = async (request, response) => {
   const {id}= request.body; 

   const post = await Post.findById(id);
   if (post){
    if(!request.file) 
    return response.status(404).json("File not found");
    const imageUrl = `${url}/file/${request.file.filename}`;
    post.image= imageUrl;
    await post.save()
    return response.status(200).json(imageUrl);
   }
   return response.json({error: "Invalid Post ID"})

}

export const getImage = async (request, response) => {
 
    try { 
          
       
        const file = await gfs.files.findOne({ filename: request.params.filename });
        // const readStream = gfs.createReadStream(file.filename);
        // readStream.pipe(response);
        const readStream = gridfsBucket.openDownloadStream(file._id);
        readStream.pipe(response);
    } catch (error) {
        response.status(500).json({ msg: error.message });
    }
}