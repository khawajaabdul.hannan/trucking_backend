
import Post from '../model/post.js';


export const createPost = async (request, response) => {
    const {title, description}= request.body;
    try {
        const post = new Post; 
        post.title= title
        post.description = description; 
        post.user= request.user._id
        await post.save();
        response.status(200).json(post._id);
    } catch (error) {
        response.status(500).json(error);
    }
}

export const updatePost = async (request, response) => {
    const {title, description}= request.body
    try {
        const post = await Post.findById(request.params.id);

        if (!post) {
            response.status(404).json({ msg: 'Post not found' })
        }
        
        post.title = title; 
        post.description= description;
        await post.save()

        response.status(200).json('post updated successfully');
    } catch (error) {
        response.status(500).json(error);
    }
}

export const deletePost = async (request, response) => {
    try {
        const post = await Post.findById(request.params.id);
        await post.delete()
        response.status(200).json('post deleted successfully');
    } catch (error) {
        response.status(500).json(error)
    }
}

export const getPost = async (request, response) => {
    try {
        const post = await Post.findById(request.params.id).select('title description image').populate("user", "name");

        response.status(200).json(post);
    } catch (error) {
        response.status(500).json(error)
    }
}

export const getAllPosts = async (request, response) => {
  
    try {
        const posts = await Post.find({}).populate("user", "name");   
        response.status(200).json(posts);
    } catch (error) {
        response.status(500).json(error)
    }
}
export const getAllEmployeePosts = async (request, response) => {
    const {_id : id} = request.user
    console.log(id);
    try {
    const posts = await Post.find({ user: id }).select('title description image').populate("user", "name");
    response.status(200).json(posts);
    } catch (error) {
        response.status(500).json(error)
    }
}