import Qoute from "../model/qoute.js";
import { request , response } from "express";


export const createQoute = async (request, response) => {
  const {name , email , address , service}= request.body; 
 

  const qoute = new Qoute({name , email , address, service}); 
  await qoute.save()

  response.status(200).json({'message' : "Qoute was logged"})
}

export const getAllQoutes = async (request, response) => {
    const qoute = await  Qoute.find({});

    response.status(200).json(qoute)
  }