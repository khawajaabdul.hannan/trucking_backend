import Ticket from "../model/ticket.js";
import { request , response } from "express";


export const createticket = async (request, response) => {
  const {title , description , type}= request.body; 
 

  const ticket = new Ticket({title , description , type}); 
  await ticket.save()

  response.status(200).json({'message' : "ticket was logged"})
}

export const getAlltickets = async (request, response) => {
    const ticket =  await Ticket.find().populate( 'user',  'name email' )
    response.status(200).json(ticket)
  }

export const updateStatus = async (request , response)=>{
  const {id}= request.params; 
  const {status}= request.body;
  const ticket = await Ticket.findById(id); 
  if (!ticket) return  response.status(404).json({'error' : "ticket was not found"})
  ticket.status= status;
  await ticket.save();
  response.status(200).json({'message' : "ticket was updated"})
}

export const GetOneTicket = async (request, response)=>{
  const {id}= request.params; 
  const ticket = await Ticket.findById(id); 
  if (!ticket) return  response.status(404).json({'error' : "ticket was not found"})
  response.status(200).json(ticket)
}

export const deleteTicket = async (request, response) => {
  try {
      const {id}= request.params; 
      const ticket = await Ticket.findById(id); 
      await ticket.delete()
      response.status(200).json('ticket deleted successfully');
  } catch (error) {
      response.status(500).json(error)
  }
}


export const employeeTickets = async (request, response) => {
  const {_id : id} = request.user
    try {
      const tickets = await Ticket.find()
      .or([
        { user: null },
        { user: { $exists: false } },
        { user: id }
      ])
      .populate("user", "name");
    response.status(200).json(tickets);
    } catch (error) {
        response.status(500).json(error)
    }
}

export const getticketCategoryTypes = (request, response)=>{
  const list = ['Poor scheduling/planning', 'Compensation', 'Mile inconsistency', 'Tractor (cab/engine) mechanical breakdown issues'  ]
  response.status(200).json(list)
}

export const getticketStatusTypes= (request, response)=>{
  const list =['pending', 'working', 'resolved']
  response.status(200).json(list)
}

