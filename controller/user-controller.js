import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import Ticket from "../model/ticket.js";
import Token from '../model/token.js'
import Employee from '../model/employee.js';

dotenv.config();

export const signUpEmployee = async (request, response) => {
    try {
        const hashedPassword = await bcrypt.hash(request.body.password, 10);
        
        const employee = { email: request.body.email, name: request.body.name, password: hashedPassword  , contact_info: request.body.phoneNumber}

        const newEmployee= new Employee(employee);
        await newEmployee.save();

        return response.status(200).json({ msg: 'Signup successfull' });
    } catch (error) {
        return response.status(500).json({ msg: 'Error while signing up user' , error   });
    }
}


export const LoginEmployee = async (request, response) => {
   
    let user = await Employee.findOne({ email: request.body.email });
    if (!user) {
        return response.status(400).json({ msg: 'Email does not match' });
    }

    try {
        let match = await bcrypt.compare(request.body.password, user.password);
        if (match) {
            const accessToken = jwt.sign(user.toJSON(), process.env.ACCESS_SECRET_KEY);
            const refreshToken = jwt.sign(user.toJSON(), process.env.REFRESH_SECRET_KEY);
            
            // const newToken = new Token({ token: refreshToken });
            // await newToken.save();
        
            response.status(200).json({ accessToken: accessToken, refreshToken: refreshToken, name: user.name, email: user.email, role: user.role });
        
        } else {
            response.status(400).json({ msg: 'Password does not match' })
        }
    } catch (error) {
        response.status(500).json({ msg: 'error while login the user' })
    }
}

export const LogoutEmployee = async (request, response) => {
    const token = request.body.token;
    await Token.deleteOne({ token: token });

    response.status(200).json({ msg: 'logout successfull' });
}

export const AssignTicket = async (request ,response )=>{
    const {id} = request.params;
    
    const ticket = await Ticket.findById(id);
    if (!ticket) return  response.status(404).json({ msg: 'Ticket was not found' })
    if (ticket.user) return  response.status(403).json({ msg: 'Ticket is already assigned to a user'});

    ticket.user= request.user._id; 
    ticket.status='working'
    await ticket.save()

    response.status(200).json({ msg: 'Ticket is assigned '});

}