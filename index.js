import express from 'express';
import dotenv from 'dotenv';
import cors from 'cors';
import bodyParser from 'body-parser';

//components
import Connection from './database/db.js';
import Router from './routes/route.js';
import TicketRoute from './routes/ticket-routes.js'
import PostRoute from './routes/post-routes.js'
import { authenticateToken } from './controller/jwt-controller.js';
import QouteRoute from "./routes/qoute-routes.js"
dotenv.config();

const app = express();

app.use(cors());
app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/', Router);
app.use('/tickets', authenticateToken ,  TicketRoute);

app.use('/posts' , PostRoute);
app.use('/qoutes', QouteRoute )

const PORT = 8000;

Connection();

app.listen(PORT, () => console.log(`Server is running successfully on PORT ${PORT}`));