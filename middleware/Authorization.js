
export const isAdmin = (request, response, next) => {
        const authRoles= ['Admin']
        if (!authRoles.includes(request.user.role))
        return response.status(403).json({message : "UnAuthorized"})
        next();
}