import mongoose from 'mongoose';

const employeeSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    contact_info : {
        type: Number, 
        required : true
    },
    role :{
        type : String, 
        enum: ['Admin', 'Employee'],
        default : 'Employee'
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
      }
});


const Employee = mongoose.model('Employee', employeeSchema);

export default Employee;