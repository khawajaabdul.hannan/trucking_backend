import mongoose from 'mongoose';

const postSchema = mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true
    },
    image: {
        type: String
      },
    createdDate: {
        type: Date
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
      },
      createdAt: {
        type: Date,
        default: Date.now
      }

});


const Post = mongoose.model('Post', postSchema);

export default Post;