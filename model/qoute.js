import mongoose from 'mongoose';

const qouteSchema = mongoose.Schema({
    name : {
        type: String,
        required: true, 
    },
    email: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    service: {
        type: String,
        enum : ['Full-service moving company', 'Hourly moving & packing services' , 'Junk Removal & Trash Pickup'],
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
      }

});


const Qoute = mongoose.model('Qoute', qouteSchema);

export default Qoute;