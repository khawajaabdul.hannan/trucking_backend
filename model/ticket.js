import mongoose from 'mongoose';

const ticketSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['Poor scheduling', 'Compensation', 'Mile inconsistency', 'Tractor mechanical breakdown issues'],
        required: true
    },
    status: {
        type: String,
        enum: ['pending', 'working', 'completed' , 'resolved'],
        default: 'pending'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employee'
    },
    createdAt: {
        type: Date,
        default: Date.now
      }
});


const Ticket =  mongoose.model('Ticket', ticketSchema);

export default Ticket;