import express from 'express';
import { createPost, updatePost, deletePost, getPost, getAllPosts , getAllEmployeePosts } from '../controller/post-controller.js';
import { Validator } from '../validator/Validator.js';
import { body , param} from 'express-validator';
const router = express.Router();
import { authenticateToken } from '../controller/jwt-controller.js';
router.post('/create', 

  [
    body('title')
    .isString().withMessage('title  should be string')
    .notEmpty().withMessage('title is required')
    .isLength({ min: 7 }).withMessage('title must be between 3 and 50 characters')
    ,
    body('description')
    .isString().withMessage('description not valid')
    .notEmpty().withMessage('description is required')
    .isLength({ min: 30 }).withMessage('description must be between 30 and 100 characters')
    ,
    Validator
  ] , authenticateToken,createPost);


router.put('/update/:id', 
  [
    body('title')
    .isString().withMessage('title  should be string')
    .notEmpty().withMessage('title is required')
    .isLength({ min: 7}).withMessage('title must be between 3 and 50 characters')
    ,
    body('description')
    .isString().withMessage('description not valid')
    .notEmpty().withMessage('description is required')
    .isLength({ min: 30 }).withMessage('description must be between 30 and 100 characters')
    ,
    Validator
  ],authenticateToken,
updatePost);

router.delete('/delete/:id', authenticateToken ,deletePost);
router.get('/employee',authenticateToken, getAllEmployeePosts);
router.get('/:id', authenticateToken,getPost);



router.get('',  getAllPosts);



export default router;