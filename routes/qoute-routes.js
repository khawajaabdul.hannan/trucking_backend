import express from 'express';
import {createQoute , getAllQoutes } from "../controller/qoute-controller.js"
const router = express.Router();
import { Validator } from '../validator/Validator.js';
import { body} from 'express-validator';


router.post('/create',
[
    body('name')
    .isString().withMessage('name  should be string')
    .notEmpty().escape().withMessage('name is required')
    .isLength({ min: 3, max: 20 }).withMessage('name must be between 3 and 50 characters')
    ,
    body('email')
    .isEmail().withMessage('email not valid')
    .notEmpty().escape().withMessage('email is required')
    ,
    body('address') 
    .isString().withMessage('address should be string')
    .notEmpty().escape().withMessage('address is required')
    .isLength({ min: 10 }).withMessage('address must have length of 13 digits')
    ,
    body('service')
    .isString().withMessage('description is should be string')
    .notEmpty().withMessage('description is required')
    .isIn(['Full-service moving company', 'Hourly moving & packing services' , 'Junk Removal & Trash Pickup']).withMessage('service does not exist')
    ,
  
    Validator
  ],  createQoute);





router.get('',  getAllQoutes);


export default router;