import express from 'express';
import { body} from 'express-validator';
import { uploadImage, getImage } from '../controller/image-controller.js';
import { signUpEmployee, LoginEmployee, LogoutEmployee  } from '../controller/user-controller.js';
import { createNewToken } from '../controller/jwt-controller.js';
import Employee from '../model/employee.js';
import {Validator} from '../validator/Validator.js'

import upload from '../utils/upload.js';

const router = express.Router();

router.post('/login',  [
    body('email')
    .isEmail().withMessage('email not valid')
    .notEmpty().escape().withMessage('email is required')
    ,
    body('password') 
    .isString().withMessage('password is should be string')
    .notEmpty().escape().withMessage('password is required')
    ,
    Validator
  ], LoginEmployee );


router.post('/signup', 
[
    body('name')
    .isString().withMessage('name  should be string')
    .notEmpty().escape().withMessage('name is required')
    .isLength({ min: 3, max: 20 }).withMessage('name must be between 3 and 50 characters')
    ,
    body('email')
    .isEmail().withMessage('email not valid')
    .notEmpty().escape().withMessage('email is required')
    .isLength({ min: 10, max: 50 }).withMessage('email must be between 7 and 10 characters')
    .custom(async (value)=>{
      const user= await Employee.findOne({email : value}); 
      if (!user) return true
      throw new Error('Unique Email Required');})
      ,

    body('password') 
    .isString().withMessage('password is should be string')
    .notEmpty().escape().withMessage('password is required')
    .isLength({ min: 5, max: 50 }).withMessage('password must be between 5 and 50 characters')
    ,
    body('phoneNumber') 
    .isString().withMessage('phoneNumber should be string')
    .notEmpty().escape().withMessage('phoneNumber is required')
    .isLength({ min: 10, max: 13 }).withMessage('phoneNumber must have length of 13 digits')
    ,
    Validator
  ], signUpEmployee);

router.post('/logout', LogoutEmployee);
router.post('/token', createNewToken);

router.post('/posts/file/upload', upload.single('file'), uploadImage);
router.get('/file/:filename', getImage);



export default router;