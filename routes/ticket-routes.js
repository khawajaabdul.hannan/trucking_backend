import { createticket , getAlltickets , GetOneTicket , updateStatus , getticketCategoryTypes , getticketStatusTypes ,deleteTicket , employeeTickets} from '../controller/ticket-controller.js';
import express from 'express';
import { AssignTicket } from '../controller/user-controller.js';
import { isAdmin } from '../middleware/Authorization.js';
import { body} from 'express-validator';
import {Validator} from '../validator/Validator.js'

const router = express.Router();

router.post('/create', isAdmin, 
[
    body('title')
    .isString().withMessage('title should be string')
    .notEmpty().escape().withMessage('title is required')
    .isLength({ min: 7 }).withMessage('title must be between 7 and 20 characters')
    ,
    body('description') 
    .isString().withMessage('description is should be string')
    .notEmpty().escape().withMessage('description is required')
    .isLength({ minlength: 10}).withMessage('description must be between 30 and 100 characters')
    ,
    body('type')
    .isString().withMessage('description is should be string')
    .notEmpty().escape().withMessage('description is required')
    .isIn(['Poor scheduling', 'Compensation', 'Mile inconsistency', 'Tractor mechanical breakdown issues']).withMessage('type does not exist'),

    Validator
  ],createticket );

router.put('/update/:id' ,  
  [
    body('status')
    .isString().withMessage('description is should be string')
    .notEmpty().escape().withMessage('description is required')
    .isIn(['pending', 'working', 'resolved' , 'completed']).withMessage('status does not exist'),

    Validator
  ], updateStatus );

router.get('/assign/:id', AssignTicket);



router.get('/',   getAlltickets );

router.get('/employee', employeeTickets)
router.get('/:id', GetOneTicket);

router.delete('/delete/:id', deleteTicket);


router.get('/category/types', getticketCategoryTypes);
router.get('/status/types', getticketStatusTypes)

export default router;